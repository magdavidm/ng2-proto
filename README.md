# README #

To get going, clone the repo '$npm install' then '$gulp'.  This will compile and generate needed files, then host the server locally.

### What is this repository for? ###

* An Node/ExpressJS server with endpoints for an [Angular2 app](https://twitchtvstreamdata.firebaseapp.com/).
* [Demo](https://ng2proto.herokuapp.com)