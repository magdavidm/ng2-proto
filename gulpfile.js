const del = require('del');
const gulp = require('gulp');
const cleanCSS = require('gulp-clean-css');
const colors  = require('colors');
const concat = require('gulp-concat');
const liveServer = require('gulp-live-server');
const plumber = require('gulp-plumber');
const runSequence = require('run-sequence');
const sass = require('gulp-sass');
const sassLint = require('gulp-sass-lint');
const sourcemaps = require('gulp-sourcemaps');
const sysBuilder = require('systemjs-builder');
const tslint = require('gulp-tslint');
const tsc = require('gulp-typescript');
const uglify = require('gulp-uglify');
const tsconfig = require('tsconfig-glob');

const tscConfig = require('./tsconfig.json');
const tsProject = tsc.createProject("tsconfig.json");

// Compile TypeScript to JS
gulp.task('compile:tsgeneric', function () {
  return tsProject
    .src()
    .pipe(tsc(tsProject.files))
    .js.pipe(gulp.dest(function(file){
      return file.base;
    })
    );
});

// Watch src files for changes, then trigger recompilation
gulp.task('watch:src', function() {
  gulp.watch('src/**/*.ts', ['scripts']);
  gulp.watch('src/**/*.scss', ['styles']);
});

// Run Express, auto rebuild and restart on src changes
gulp.task('serve', ['watch:src'], function () {
  var server = liveServer.new('server.js');
  server.start();

  gulp.watch('server.js', server.start.bind(server));
});

gulp.task('ts', function(callback){
  runSequence('compile:tsgeneric', callback);
});

gulp.task('default', function(callback) {
  runSequence('ts', 'serve', callback);
});