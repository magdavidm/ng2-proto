"use strict";
//"use strict";
var System = require("systemjs");
// Services
var twitch_server_service_1 = require("./server/twitch-server.service");
var twitchServerService = new twitch_server_service_1.TwitchService();
var path = require('path');
var express = require("express");
var app = express();
var rootPath = path.normalize(__dirname);
app.set("port", (process.env.PORT || 8080));
// for static files
app.use('', express.static(rootPath + "/public"));
app.get("/streams/:term", function (req, res) {
    console.log("searching...");
    var searchTerm = req.params.term;
    twitchServerService.retrieveStreamData(searchTerm, function (body, err) {
        console.log("done");
        if (err)
            throw err;
        res.set({
            "Access-Control-Allow-Origin": '*'
        });
        res.send(body);
    });
});
app.get('/dashboard', function (req, res) {
    res.send("Server dashboard Response");
});
// anything else, send the NG2 app
app.get("*", function (req, res) {
    console.log("sending application");
    res.sendFile(rootPath + "/app/index.html");
});
app.listen(app.get("port"), function () {
    console.log("Node app is running on port", app.get("port"));
});
